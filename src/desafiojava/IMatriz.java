package desafiojava;

public interface IMatriz {
    public void setM1(int[][] mat);
    public void setM2(int[][] mat);
    public int[][] multiplica() throws Exception;
}
