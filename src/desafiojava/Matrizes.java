package desafiojava;

import java.util.Random;

public class Matrizes {
        
    public static int[][] novaMatriz(int linhas,int colunas) {
        return new int[linhas][colunas];
    }
    
    public static int[][] matrizRandomica(int linhas, int colunas) {
        int[][] mat = novaMatriz(linhas,colunas);
        //Random rdr = new Random(Calendar.getInstance().getTimeInMillis());
        Random rdr = new Random();
        for(int i = 0; i < linhas; ++i) {
            for(int j = 0; j < colunas; ++j) {
                mat[i][j] = rdr.nextInt(500);
            }
        }
        return mat;
    }
    
    public static void imprime(int[][] mat) {
        for(int i = 0; i < mat.length; ++i) {
            for(int j = 0; j < mat[0].length; ++j) {
                System.out.print(String.format("%d ", mat[i][j]));
            }
            System.out.println("");
        }
    }
   
    public static int produtoLinhaColuna(int[][] m1, int[][]m2, int linha, int coluna) {
        int result = 0; 
        for(int i = 0; i < m1[linha].length; ++i) {
            result += m1[linha][i] * m2[i][coluna];
        }
        return result;
    }
   
}
